package com.test.passwordvalidator.api.cucumber;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TestConfig {

    @Autowired
    private ObjectMapper objectMapper;

    @Bean
    @Primary
    public MockRestServiceServer mockRestServiceServer(RestTemplate restTemplate) {
        return MockRestServiceServer.bindTo(restTemplate).ignoreExpectOrder(true).build();
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
