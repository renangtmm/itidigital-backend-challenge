package com.test.passwordvalidator.api.cucumber.defs;
;
import io.cucumber.java.pt.Entao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class HttpRequestDefs extends SpringBootIntegrationTest {

    private MockMvc mvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private ResultActions requestResult;

    private MultiValueMap<String, String> params = new LinkedMultiValueMap<>();

    public ResultActions getRequestResult() {
        return requestResult;
    }

    public ResultActions mvcPerform(MockHttpServletRequestBuilder request) throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        this.requestResult = mvc.perform(request);
        return requestResult;
    }

    public void setRequestResult(ResultActions requestResult) {
        this.requestResult = requestResult;
    }

    public MultiValueMap<String, String> getConfiguredParams() {
        return params;
    }

    @Entao("^o status de resposta deve ser \"(\\d+)\"$")
    public void verifyStatusCode(int status) throws Exception {
        requestResult.andExpect(status().is(status));
    }

}
