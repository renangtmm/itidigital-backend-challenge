package com.test.passwordvalidator.api.cucumber.defs;

import com.test.passwordvalidator.PasswordValidatorApplication;
import com.test.passwordvalidator.api.cucumber.CucumberConfig;
import com.test.passwordvalidator.api.cucumber.TestConfig;
import io.cucumber.spring.CucumberContextConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

@CucumberContextConfiguration
@SpringBootTest(classes = {PasswordValidatorApplication.class, TestConfig.class, CucumberConfig.class})
public abstract class SpringBootIntegrationTest {

}
