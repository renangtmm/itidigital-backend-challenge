package com.test.passwordvalidator.api.cucumber.defs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.passwordvalidator.api.password.mappers.PasswordValidationRuleMessage;
import com.test.passwordvalidator.api.password.requests.PasswordValidationRequest;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class PasswordValidateDefs extends SpringBootIntegrationTest {

    @Autowired
    private HttpRequestDefs httpRequestDefs;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private MessageSource messageSource;

    @Quando("^executar o post para validar a senha totalmente \"(.*)\"$")
    public void executarOPostParaValidarASenhaTotalmente(String password) throws Exception {

        PasswordValidationRequest passwordValidationRequest = new PasswordValidationRequest();
        passwordValidationRequest.setPassword("null".equals(password) ? null : password);

        httpRequestDefs.mvcPerform(post("/password/validateFully")
                .content(this.mapper.writeValueAsString(passwordValidationRequest))
                .contentType(MediaType.APPLICATION_JSON)
        );

    }

    @Quando("^executar o post para validar a senha \"(.*)\"$")
    public void executarOPostParaValidarASenha(String password) throws Exception {

        PasswordValidationRequest passwordValidationRequest = new PasswordValidationRequest();
        passwordValidationRequest.setPassword("null".equals(password) ? null : password);

        httpRequestDefs.mvcPerform(post("/password/validate")
                .content(this.mapper.writeValueAsString(passwordValidationRequest))
                .contentType(MediaType.APPLICATION_JSON)
        );

    }

    @Entao("^a resposta deve ser de senha valida")
    public void aRespostaDeveSerDeSenhaValida() throws Exception {

        this.httpRequestDefs.getRequestResult().andExpect(jsonPath("$.is_valid")
                .value("true"));

    }

    @Entao("^a resposta deve ser de senha invalida")
    public void aRespostaDeveSerDeSenhaInvalida() throws Exception {

        this.httpRequestDefs.getRequestResult().andExpect(jsonPath("$.is_valid")
                .value("false"));

    }

    @Entao("^a senha deve ser invalida pelo motivo \"(.*)\"$")
    public void aSenhaDeveSerInvalidaPeloMotivo(
            PasswordValidationRuleMessage passwordValidationRuleMessage) throws Exception {

        String message = this.messageSource.getMessage(passwordValidationRuleMessage.getMessageKey(), new Object[0], LocaleContextHolder.getLocale());

        this.httpRequestDefs.getRequestResult().andExpect(jsonPath("$.invalid_reasons")
                .value(new BaseMatcher<Object>() {
                    @Override
                    public boolean matches(Object object) {
                        return object.toString().contains(message);
                    }

                    @Override
                    public void describeTo(Description description) {
                    }
                }));

    }

}
