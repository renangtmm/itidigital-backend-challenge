package com.test.passwordvalidator.api.cucumber;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = { "com.test.passwordvalidator.api.cucumber" },
        features = "src/test/resources")
public class CucumberConfig {


}
