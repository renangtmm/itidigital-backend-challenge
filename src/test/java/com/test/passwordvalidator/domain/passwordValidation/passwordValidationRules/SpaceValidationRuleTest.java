package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class SpaceValidationRuleTest {

    @Autowired
    private SpaceValidationRule spaceValidationRule;
    
    @Test
    public void isValidTest(){

        assertTrue(spaceValidationRule.isValid("ab1234fgh"));
        assertTrue(spaceValidationRule.isValid("abcferwfgh"));
        assertTrue(spaceValidationRule.isValid("abcE$##gh"));
        assertTrue(spaceValidationRule.isValid("abcdefgh"));

        assertFalse(spaceValidationRule.isValid("te ste"));
        assertFalse(spaceValidationRule.isValid("tes te"));
        assertFalse(spaceValidationRule.isValid("abcdefghi "));
        assertFalse(spaceValidationRule.isValid(" 8ue123r7832r"));

    }

}
