package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class RepeatValidationRuleTest {

    @Autowired
    private RepeatValidationRule repeatValidationRule;

    @Test
    public void isValidTest(){

        assertTrue(repeatValidationRule.isValid("abcde"));
        assertTrue(repeatValidationRule.isValid("12345"));
        assertTrue(repeatValidationRule.isValid("!@wefrA"));
        assertTrue(repeatValidationRule.isValid("7*qfr"));

        assertFalse(repeatValidationRule.isValid("abcdeafgh"));
        assertFalse(repeatValidationRule.isValid("abcdefghh"));
        assertFalse(repeatValidationRule.isValid("abbcdefgh"));
        assertFalse(repeatValidationRule.isValid("abcdefgh11"));
        assertFalse(repeatValidationRule.isValid("abc@def@gh"));

    }

}
