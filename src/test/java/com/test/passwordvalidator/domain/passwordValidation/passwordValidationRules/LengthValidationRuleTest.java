package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class LengthValidationRuleTest {

    @Autowired
    private LengthValidationRule lengthValidationRule;

    @Test
    public void isValidTest(){

        assertTrue(lengthValidationRule.isValid("123456789111"));
        assertTrue(lengthValidationRule.isValid("12345678910"));
        assertTrue(lengthValidationRule.isValid("123456789"));

        assertFalse(lengthValidationRule.isValid("1234567"));
        assertFalse(lengthValidationRule.isValid("123456"));
        assertFalse(lengthValidationRule.isValid("12345"));

    }

}
