package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class LowerCaseValidationRuleTest {

    @Autowired
    private LowerCaseValidationRule lowerCaseValidationRule;


    @Test
    public void isValidTest(){

        assertTrue(lowerCaseValidationRule.isValid("aeiou"));
        assertTrue(lowerCaseValidationRule.isValid("aeiou4231"));
        assertTrue(lowerCaseValidationRule.isValid("aeioferfer"));
        assertTrue(lowerCaseValidationRule.isValid("aeioue2e23"));
        assertTrue(lowerCaseValidationRule.isValid("aeiou!@#$ "));

        assertFalse(lowerCaseValidationRule.isValid("AAADAF5"));
        assertFalse(lowerCaseValidationRule.isValid("XEFAAFE34R3"));
        assertFalse(lowerCaseValidationRule.isValid("ASF5DF2134"));
        assertFalse(lowerCaseValidationRule.isValid("DEF1234FGT"));
        assertFalse(lowerCaseValidationRule.isValid("HRR12646"));

    }

}
