package com.test.passwordvalidator.domain.passwordValidation;

import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.LengthValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.LowerCaseValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.LowerCaseValidationRuleTest;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.OneDigitValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.RepeatValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.SpaceValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.SpecialCharacterValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.UpperCaseValidationRule;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PasswordValidationServiceTest {

    @Autowired
    private PasswordValidationService passwordValidationService;

    @Test
    public void validatePasswordTest() {

        this.validatePassword("", false,
                 Arrays.asList(
                        LengthValidationRule.class,
                        LowerCaseValidationRule.class,
                        SpecialCharacterValidationRule.class,
                        UpperCaseValidationRule.class,
                        OneDigitValidationRule.class
                ));

        this.validatePassword("aa", false,
                 Arrays.asList(
                        RepeatValidationRule.class,
                        LengthValidationRule.class,
                        SpecialCharacterValidationRule.class,
                        UpperCaseValidationRule.class,
                        OneDigitValidationRule.class
                ));

        this.validatePassword("ab", false,
                 Arrays.asList(
                        LengthValidationRule.class,
                        SpecialCharacterValidationRule.class,
                        UpperCaseValidationRule.class,
                        OneDigitValidationRule.class
                ));

        this.validatePassword("AAAbbbCc", false,
                 Arrays.asList(
                        RepeatValidationRule.class,
                        LengthValidationRule.class,
                        SpecialCharacterValidationRule.class,
                        OneDigitValidationRule.class
                ));

        this.validatePassword("AbTp9!foo", false,
                 Arrays.asList(
                        RepeatValidationRule.class
                ));

        this.validatePassword("AbTp9!foA", false,
                 Arrays.asList(
                        RepeatValidationRule.class
                ));

        this.validatePassword("AbTp9 fok", false,
                  Arrays.asList(
                        SpaceValidationRule.class,
                        SpecialCharacterValidationRule.class
                ));

        this.validatePassword("AbTp9!fok", true);

    }

    private void validatePassword(String password, boolean expectValid) {
        this.validatePassword(password,expectValid,Arrays.asList());
    }

    private void validatePassword(String password, boolean expectValid,
                                  List<Class<? extends PasswordValidationRule>> expectNotPassedRules) {

        PasswordValidationResult completeValidationResult = this.passwordValidationService.validatePasswordFully(password);
        assertEquals(expectValid, completeValidationResult.isValid());
        assertTrue(completeValidationResult.getNotPassedRules().containsAll(expectNotPassedRules));

        PasswordValidationResult simpleValidationResult = this.passwordValidationService.validatePassword(password);
        assertEquals(expectValid,simpleValidationResult.isValid());

    }

}
