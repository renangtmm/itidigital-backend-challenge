package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class UpperCaseValidationRuleTest {

    @Autowired
    private UpperCaseValidationRule upperCaseValidationRule;


    @Test
    public void isValidTest(){

        assertTrue(upperCaseValidationRule.isValid("AEIOU"));
        assertTrue(upperCaseValidationRule.isValid("Teste"));
        assertTrue(upperCaseValidationRule.isValid("r42rf56h6II"));
        assertTrue(upperCaseValidationRule.isValid("dwefrTTeereT"));
        assertTrue(upperCaseValidationRule.isValid("Ret44"));

        assertFalse(upperCaseValidationRule.isValid("aeiou"));
        assertFalse(upperCaseValidationRule.isValid("teste"));
        assertFalse(upperCaseValidationRule.isValid("1234"));
        assertFalse(upperCaseValidationRule.isValid("@#$%%"));

    }

}
