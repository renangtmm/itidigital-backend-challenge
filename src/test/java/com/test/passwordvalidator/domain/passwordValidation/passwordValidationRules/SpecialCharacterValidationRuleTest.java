package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class SpecialCharacterValidationRuleTest {

    @Autowired
    private SpecialCharacterValidationRule specialCharacterValidationRule;


    @Test
    public void isValidTest(){

        assertTrue(specialCharacterValidationRule.isValid("AEIOU$!"));
        assertTrue(specialCharacterValidationRule.isValid("AE@OU!"));
        assertTrue(specialCharacterValidationRule.isValid("A@#$"));
        assertTrue(specialCharacterValidationRule.isValid("AE$%$!"));

        assertFalse(specialCharacterValidationRule.isValid("a23ee"));
        assertFalse(specialCharacterValidationRule.isValid("ae1234"));
        assertFalse(specialCharacterValidationRule.isValid("454266WE"));
        assertFalse(specialCharacterValidationRule.isValid("aeSEr"));

    }

}
