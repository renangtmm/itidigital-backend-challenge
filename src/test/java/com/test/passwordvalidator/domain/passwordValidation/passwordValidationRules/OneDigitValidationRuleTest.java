package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class OneDigitValidationRuleTest {

    @Autowired
    private OneDigitValidationRule oneDigitValidationRule;

    @Test
    public void isValidTest(){

        assertTrue(oneDigitValidationRule.isValid("12345"));
        assertTrue(oneDigitValidationRule.isValid("AE234IU"));
        assertTrue(oneDigitValidationRule.isValid("12344"));
        assertTrue(oneDigitValidationRule.isValid("AE##r4"));
        assertTrue(oneDigitValidationRule.isValid("Adedef33"));

        assertFalse(oneDigitValidationRule.isValid("aeeou"));
        assertFalse(oneDigitValidationRule.isValid("agregE#$R$Aou"));
        assertFalse(oneDigitValidationRule.isValid("aFRNU#Ue"));
        assertFalse(oneDigitValidationRule.isValid("ae#$%#A"));
        assertFalse(oneDigitValidationRule.isValid("ae@def-"));

    }

}
