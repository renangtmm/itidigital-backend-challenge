#language: pt

Funcionalidade: Validacao de senha

  Esquema do Cenario: Validar uma senha valida
    Quando executar o post para validar a senha totalmente "<senha>"
    Entao o status de resposta deve ser "200"
    E a resposta deve ser de senha valida
    Exemplos:
      | senha      |
      | A$a123456  |
      | a$B123456  |
      | A$a1@sBCD  |

  Cenario: Validar uma senha invalida por todos os motivos
    Quando executar o post para validar a senha "   "
    Entao o status de resposta deve ser "200"
    E a resposta deve ser de senha invalida

  Cenario: Validar uma senha invalida por repetir caracteres
    Quando executar o post para validar a senha "AA1$a123456"
    Entao o status de resposta deve ser "200"
    E a resposta deve ser de senha invalida

  Cenario: Validar uma senha invalida por nao conter letra minuscula e repetir caracteres
    Quando executar o post para validar a senha "AA1$A123456"
    Entao o status de resposta deve ser "200"
    E a resposta deve ser de senha invalida

  Cenario: Validar uma senha invalida por nao conter letra minuscula e um digito
    Quando executar o post para validar a senha "$ABCDEFGHI"
    Entao o status de resposta deve ser "200"
    E a resposta deve ser de senha invalida

  Cenario: Tentativa de validar uma senha nula
    Quando executar o post para validar a senha "null"
    Entao o status de resposta deve ser "400"
