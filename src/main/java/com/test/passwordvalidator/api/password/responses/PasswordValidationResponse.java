package com.test.passwordvalidator.api.password.responses;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PasswordValidationResponse {

    @JsonProperty("is_valid")
    private boolean isValid;

    @JsonProperty("invalid_reasons")
    private List<String> invalidReasons;

    @JsonIgnore
    public PasswordValidationResponse withoutReasons(){
        this.invalidReasons = null;
        return this;
    }

}
