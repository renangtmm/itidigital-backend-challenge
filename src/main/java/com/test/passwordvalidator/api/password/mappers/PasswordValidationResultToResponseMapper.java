package com.test.passwordvalidator.api.password.mappers;

import com.test.passwordvalidator.api.password.responses.PasswordValidationResponse;
import com.test.passwordvalidator.domain.passwordValidation.PasswordValidationResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class PasswordValidationResultToResponseMapper {

    @Autowired
    private MessageSource messageSource;

    public PasswordValidationResponse map(PasswordValidationResult passwordValidationResult) {

        Locale locale = LocaleContextHolder.getLocale();

        return PasswordValidationResponse.builder()
                .isValid(passwordValidationResult.isValid())
                .invalidReasons(passwordValidationResult.isValid() ? null :
                        passwordValidationResult.getNotPassedRules().stream().map(rule ->
                                this.messageSource.getMessage(PasswordValidationRuleMessage.fromRuleClass(rule).getMessageKey(), new Object[0], locale)
                        ).collect(Collectors.toList()))
                .build();

    }

}
