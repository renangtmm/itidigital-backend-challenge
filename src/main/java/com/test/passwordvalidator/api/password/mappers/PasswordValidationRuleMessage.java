package com.test.passwordvalidator.api.password.mappers;

import com.test.passwordvalidator.domain.passwordValidation.PasswordValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.LengthValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.LowerCaseValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.OneDigitValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.RepeatValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.SpaceValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.SpecialCharacterValidationRule;
import com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules.UpperCaseValidationRule;
import lombok.Getter;

import java.util.Objects;

@Getter
public enum PasswordValidationRuleMessage {

    LENGTH_VALIDATION_RULE(LengthValidationRule.class,"password.validation.rules.length"),
    LOWER_CASE_VALIDATION_RULE(LowerCaseValidationRule.class,"password.validation.rules.lower-case"),
    UPPER_CASE_VALIDATION_RULE(UpperCaseValidationRule.class,"password.validation.rules.upper-case"),
    ONE_DIGIT_VALIDATION_RULE(OneDigitValidationRule.class,"password.validation.rules.one-digit"),
    REPEAT_VALIDATION_RULE(RepeatValidationRule.class,"password.validation.rules.repeat"),
    SPACE_VALIDATION_RULE(SpaceValidationRule.class,"password.validation.rules.space"),
    SPECIAL_CHARACTER_VALIDATION_RULE(SpecialCharacterValidationRule.class,"password.validation.rules.special-character"),
    UNDEFINED(null,"password.validation.rules.undefined");

    private Class<? extends PasswordValidationRule> rule;
    private String messageKey;

    private PasswordValidationRuleMessage(Class<? extends PasswordValidationRule> rule, String messageKey){

        this.rule = rule;
        this.messageKey = messageKey;

    }

    public static PasswordValidationRuleMessage fromRuleClass(Class<? extends PasswordValidationRule> ruleClass){
        Objects.requireNonNull(ruleClass);

        for(PasswordValidationRuleMessage passwordValidationRuleMessage: PasswordValidationRuleMessage.values()){
            if(ruleClass.equals(passwordValidationRuleMessage.getRule())){
                return passwordValidationRuleMessage;
            }
        }

        return UNDEFINED;

    }

}
