package com.test.passwordvalidator.api.password;

import com.test.passwordvalidator.api.password.mappers.PasswordValidationResultToResponseMapper;
import com.test.passwordvalidator.api.password.requests.PasswordValidationRequest;
import com.test.passwordvalidator.api.password.responses.PasswordValidationResponse;
import com.test.passwordvalidator.domain.passwordValidation.PasswordValidationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/password")
public class PasswordResource {

    @Autowired
    private PasswordValidationService passwordValidationService;

    @Autowired
    private PasswordValidationResultToResponseMapper passwordValidationResultToResponseMapper;

    @ApiOperation(value = "Valida uma senha completamente",
            notes = "Valida a senha, e no caso de ser invalida, retorna o(s) motivo(s).")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validacao de senha executada.", response = PasswordValidationResponse.class),
            @ApiResponse(code = 400, message = "Erro na validacao da senha.")
    })
    @PostMapping(value = "/validateFully",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PasswordValidationResponse> validateFully(
            @RequestBody @Valid PasswordValidationRequest passwordValidationRequest) {

        PasswordValidationResponse response = this.passwordValidationResultToResponseMapper.map(
                this.passwordValidationService.validatePasswordFully(passwordValidationRequest.getPassword()));

        return ResponseEntity.status(HttpStatus.OK)
                .body(response);

    }

    @ApiOperation(value = "Valida uma senha",
                    notes = "Valida a senha")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Validacao de senha executada.", response = PasswordValidationResponse.class),
            @ApiResponse(code = 400, message = "Erro na validacao da senha.")
    })
    @PostMapping(value = "/validate",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PasswordValidationResponse> validate(
            @RequestBody @Valid PasswordValidationRequest passwordValidationRequest) {

        PasswordValidationResponse response = this.passwordValidationResultToResponseMapper.map(
                this.passwordValidationService.validatePassword(passwordValidationRequest.getPassword()))
                .withoutReasons();

        return ResponseEntity.status(HttpStatus.OK)
                .body(response);

    }

}
