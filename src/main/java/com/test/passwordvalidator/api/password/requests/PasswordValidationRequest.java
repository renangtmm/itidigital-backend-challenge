package com.test.passwordvalidator.api.password.requests;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class PasswordValidationRequest {

    @NotNull
    private String password;

}

