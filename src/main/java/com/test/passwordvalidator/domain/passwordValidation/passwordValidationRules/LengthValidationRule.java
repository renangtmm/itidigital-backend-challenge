package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import com.test.passwordvalidator.domain.passwordValidation.PasswordValidationRule;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(0)
@Component
public class LengthValidationRule implements PasswordValidationRule {
    @Override
    public boolean isValid(String password) {
        return password.length() >= 9;
    }
}
