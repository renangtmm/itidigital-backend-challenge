package com.test.passwordvalidator.domain.passwordValidation;

public interface PasswordValidationRule {

    public boolean isValid(String password);

}
