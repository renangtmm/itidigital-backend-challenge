package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(3)
@Component
public class UpperCaseValidationRule extends RegexPasswordValidationRule {

    public UpperCaseValidationRule() {

        super(".*[A-Z].*");

    }

}
