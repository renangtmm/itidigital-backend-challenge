package com.test.passwordvalidator.domain.passwordValidation;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class PasswordValidationServiceImpl implements PasswordValidationService {

    @Autowired
    private Set<PasswordValidationRule> passwordValidationRules;

    @Autowired
    @Qualifier("passwordValidationTaskExecutor")
    private TaskExecutor taskExecutor;


    /**
     * Valida a senha passando por uma regra de cada vez de forma sincrona, e caso
     * uma delas seja invalida ja iterrompe a execucao retornando o primeiro problema.
     *
     * @param password Senha a ser validada
     *
     * @return Resultado da validacao que indica se a senha é valida
     * e caso contrário aponta o primeiro problema encontrado.
     */
    @Override
    public PasswordValidationResult validatePassword(String password) {
        Objects.requireNonNull(password);

        PasswordValidationResult result = new PasswordValidationResult();

        for (PasswordValidationRule rule : this.passwordValidationRules) {

            boolean isValidRuleResult = false;

            try {
                isValidRuleResult = rule.isValid(password);
            } catch (Exception ex) {
                log.error("stage=password-validation-rule-error, rule={}, error={}", rule.getClass().getSimpleName(), ex.toString());
            }

            if (!isValidRuleResult && result.addNotPassedRule(rule).isInvalid()) {
                break;
            }

        }

        return result;

    }

    /**
     * Valida a senha passando por todas as regras, executa as validações em paralelo
     * a fim de otimizar o desempenho da validacao, principalmente caso as regras
     * de validação venham a ficar mais complexas
     *
     * @param password Senha a ser validada
     *
     * @return Resultado da validacao que indica se a senha é valida
     * e caso contrário aponta todos os problemas que foram encontrados.
     */
    @Override
    public PasswordValidationResult validatePasswordFully(final String password) {
        Objects.requireNonNull(password);

        PasswordValidationResult result = new PasswordValidationResult();

        CompletableFuture.allOf(this.passwordValidationRules.stream().map(rule ->
                CompletableFuture.supplyAsync(() -> rule.isValid(password), this.taskExecutor)
                        .exceptionally(ex -> {
                            log.error("stage=password-validation-rule-error, rule={}, error={}", rule.getClass().getSimpleName(), ex.toString());
                            return false;
                        })
                        .thenAccept(isValid -> {
                            if(!isValid) {
                                synchronized (result) {
                                    result.addNotPassedRule(rule);
                                }
                            }
                        })).toArray(CompletableFuture[]::new))
                .join();

        return result;
    }

}
