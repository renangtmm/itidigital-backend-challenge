package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(2)
@Component
public class LowerCaseValidationRule extends RegexPasswordValidationRule {

    public LowerCaseValidationRule() {

        super(".*[a-z].*");

    }

}
