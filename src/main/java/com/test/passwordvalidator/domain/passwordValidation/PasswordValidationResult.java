package com.test.passwordvalidator.domain.passwordValidation;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.HashSet;
import java.util.Set;

@Getter
@ToString
@EqualsAndHashCode
public class PasswordValidationResult {

    private boolean valid;
    private Set<Class<? extends PasswordValidationRule>> notPassedRules;

    protected PasswordValidationResult(){
        this.notPassedRules = new HashSet<>();
        this.valid = true;
    }

    protected PasswordValidationResult addNotPassedRule(PasswordValidationRule passwordValidationRule){

        this.notPassedRules.add(passwordValidationRule.getClass());
        this.valid = false;

        return this;
    }

    public boolean isInvalid(){
    	
    	return !this.isValid();

    }

}
