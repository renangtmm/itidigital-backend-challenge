package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(5)
@Component
public class RepeatValidationRule extends RegexPasswordValidationRule {

    public RepeatValidationRule() {

        super(".*(.).*(\\1).*", false);

    }

}
