package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(6)
@Component
public class SpaceValidationRule extends RegexPasswordValidationRule {

    public SpaceValidationRule() {

        super("[^ ]*");

    }

}
