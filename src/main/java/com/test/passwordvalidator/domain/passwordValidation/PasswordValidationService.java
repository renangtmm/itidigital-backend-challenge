package com.test.passwordvalidator.domain.passwordValidation;

public interface PasswordValidationService {

    public PasswordValidationResult validatePassword(String password);

    public PasswordValidationResult validatePasswordFully(String password);

}
