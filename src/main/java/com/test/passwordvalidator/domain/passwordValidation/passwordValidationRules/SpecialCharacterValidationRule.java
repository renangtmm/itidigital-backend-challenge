package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(4)
@Component
public class SpecialCharacterValidationRule extends RegexPasswordValidationRule {

    public SpecialCharacterValidationRule() {

        super(".*[!@#$%^&*()\\-+].*");

    }

}
