package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(1)
@Component
public class OneDigitValidationRule extends RegexPasswordValidationRule {

    public OneDigitValidationRule() {

        super(".*[0-9].*");

    }

}
