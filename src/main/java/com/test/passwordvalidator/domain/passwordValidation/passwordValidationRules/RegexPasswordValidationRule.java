package com.test.passwordvalidator.domain.passwordValidation.passwordValidationRules;

import com.test.passwordvalidator.domain.passwordValidation.PasswordValidationRule;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class RegexPasswordValidationRule implements PasswordValidationRule {

    private String regex;

    //informa se o regex tem que dar match ou nao, para ser valido
    private boolean match;

    public RegexPasswordValidationRule(String regex){
        this(regex,true);
    }

    @Override
    public boolean isValid(String password){
        return password.matches(this.regex) ^ !match;
    }

}
