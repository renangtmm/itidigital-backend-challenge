﻿## Descrição 

Projeto referente a situação descrita nesse repositório https://github.com/itidigital/backend-challenge ,
se trata de uma API, que oferece o endpoint **/password/validate** e **/password/validateFully**, ambos
fazem o que é proposto no problema (porém de forma ligeiramente diferente), validando 
a senha conforme os seguintes critérios:

   - Nove ou mais caracteres
   - Ao menos 1 dígito
   - Ao menos 1 letra minúscula
   - Ao menos 1 letra maiúscula
   - Ao menos 1 caractere especial
   - Não possuir caracteres repetidos dentro do conjunto
   - Espaços em branco não devem ser considerados como caracteres válidos.

A aplicação conta com Swagger, que pode ser acessado localmente enquanto estiver em execução, em [http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/) para visualização
de mais detalhes técnicos sobre os endpoints, mas basicamente funcionam da seguinte forma:

**POST /password/validate:**
 
 
Simplesmente retorna o resultado da validação conforme manda o problema:
   
   - Request:
```   
{
   "password":"123456"
 }
```
    
   - Response:
```    
{
  "is_valid": false
}
```

**POST /password/validateFully:**
 
Retorna o resultado da validação, e também os motivos da invalidade da senha no caso dela ser invalida:

>OBS: Acrescentei esse endpoint porque a forma como foi implementado favoreceu a disponibilização do mesmo, e oferece mais possibilidades para as aplicações que forem realizar a integração.
   
   - Request:
```   
{
   "password":"123456"
 }
```
    
   - Response:
```    
{
  "is_valid": false,
  "invalid_reasons": [
        "A senha deve ter 9 ou mais caracteres",
        "A senha deve ter ao menos 1 letra minuscula",
        "A senha deve ter ao menos 1 caractere especial",
        "A senha deve ter ao menos 1 letra maiuscula"
      ]
}
```

## Tecnologias

 - **Java 11**
 - **Gradle**
 - **Spring Boot**
 - **Spring Sleuth**
 - **Slf4j**
 - **Loombok**
 - **Cucumber**
 - **JUnit**
 - **Actuator**
 - **Swagger**
 
 IDE: **Inteli J**

## Alguns pontos da estrutura

- Separa as regras de validação por classe, todas devem implementar a interface **PasswordValidationRule** 

- A classe ***PasswordValidationServiceImpl*** recebe as implementações de  **PasswordValidationRule** via injeção de dependencia, favorecendo o baixo acomplamento 

- A validação completa (PasswordValidationServiceImpl#**validatePasswordFully**)  executa todas as regras simultaneamente (em threads separadas), favorecendo a melhoria de desempenho, e possível aumento da complexidade de validação, que o modelo permite. 

- A validação comum (PasswordValidationServiceImpl#**validatePassword**), executa cada regra na mesma thread para conseguir iterromper as outras regras ao encontrar a primeira inválida.

- Separação da regra de negócio no pacote **domain** e referentes a api no pacote **api**
> Não vi necessidade de separar em 2 projetos, porque ja me parece algo muito pequeno, contúdo num cenário real teria de avaliar a possibilidade de crescimento.


## Testes

Foram escritos testes unitários e integrados, para rodar todos os testes basta executar o comando na pasta do projeto: 

```
./gradlew test
```

- Os arquivos de teste **integrado** se encontram na pasta **/test/resources**, eles validam o comportamento da API como um todo, foram escritos em forma de BDD em Gherkin, estão separados por endpoint.

- Os testes **unitários** se encontram na pasta **/test/java** conforme padrão.


## Executando localmente

Para rodar o projeto na maquina local, é necessário ter o gradle e JDK 11, e com isso basta rodar o comando dentro da pasta do projeto: 

```
./gradlew bootRun
```

Se tudo der certo a API ira iniciar na porta 8080. 
 

